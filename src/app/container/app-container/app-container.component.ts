import { Component, OnInit } from '@angular/core';
import { CommunicationService } from 'src/app/shared/services/communication/communication.service';
import { AuthService } from 'src/app/shared/services/authService/auth.service';

@Component({
  selector: 'app-app-container',
  templateUrl: './app-container.component.html',
  styleUrls: ['./app-container.component.scss']
})
export class AppContainerComponent implements OnInit {

  checkoutFoodList: any;
  foodItemsCount = 0;

  constructor(private communication: CommunicationService, private authService: AuthService) { }

  ngOnInit() {
    this.communication.getFoodCheckoutData().subscribe(foodData => {
      if (foodData) {
        this.checkoutFoodList = foodData;
        this.foodItemsCount = this.checkoutFoodList.size;
      }
    });
  }

  logout() {
    this.authService.logout();
  }
}
