import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppContainerComponent } from './container/app-container/app-container.component';
import { SharedModule } from './shared/shared.module';
import { LoginComponent } from './views/login/login.component';
import { InternalServerErrorComponent } from './views/internal-server-error/internal-server-error.component';
import { PageNotFoundErrorComponent } from './views/page-not-found-error/page-not-found-error.component';


const APP_CONTAINERS = [
  AppContainerComponent
];


@NgModule({
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    LoginComponent,
    InternalServerErrorComponent,
    PageNotFoundErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
