import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FoodDashboardComponent } from './food-dashboard/food-dashboard.component';
import { FoodItemsComponent } from './food-items/food-items.component';


const routes: Routes = [
  {
    path: '',
    component: FoodDashboardComponent
  },
  {
    path: 'food/:id',
    component: FoodItemsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FoodDashboardRoutingModule { }
