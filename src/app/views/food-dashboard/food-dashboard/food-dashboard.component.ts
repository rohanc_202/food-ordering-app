import { Component, OnInit } from '@angular/core';
import { FoodOrderDataService } from 'src/app/shared/services/foodOrderData/food-order-data.service';
import {Food} from './../../../shared/interfaces/interfaces';
import { Router } from '@angular/router';

@Component({
  selector: 'app-food-dashboard',
  templateUrl: './food-dashboard.component.html',
  styleUrls: ['./food-dashboard.component.scss']
})
export class FoodDashboardComponent implements OnInit {

  foodList: Food[];

  constructor(private foodOrderData: FoodOrderDataService, private router: Router ) { }

  ngOnInit() {
    this.getFoodList();
  }

  goToFoodItemsList(foodItem) {
    this.router.navigate(['/food', foodItem.id]);
  }

  getFoodList() {
    this.foodOrderData.getData('foodList.json').subscribe(result => {
      this.foodList = result;
    });
  }

}
