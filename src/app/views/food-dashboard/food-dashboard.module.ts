import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FoodDashboardRoutingModule } from './food-dashboard-routing.module';
import { FoodDashboardComponent } from './food-dashboard/food-dashboard.component';
import { FoodItemsComponent } from './food-items/food-items.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [FoodDashboardComponent, FoodItemsComponent],
  imports: [
    CommonModule,
    FoodDashboardRoutingModule,
    SharedModule
  ]
})
export class FoodDashboardModule { }
