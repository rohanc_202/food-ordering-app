import { Component, OnInit } from '@angular/core';
import { FoodOrderDataService } from 'src/app/shared/services/foodOrderData/food-order-data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FoodItemsService } from './food-items.service';

@Component({
  selector: 'app-food-items',
  templateUrl: './food-items.component.html',
  styleUrls: ['./food-items.component.scss']
})
export class FoodItemsComponent implements OnInit {

  foodData: any;
  foodId: string;

  constructor(private foodOrderData: FoodOrderDataService, private router: Router, private route: ActivatedRoute,
              private foodItemsService: FoodItemsService) { }

  ngOnInit() {
    this.getFoodType();
  }

  getFoodType() {
    this.route.params.subscribe(url => {
      this.foodId = url.id;
      this.getFoodItemsList(parseInt(this.foodId, 10));
    });
  }

  getFoodItemsList(foodId: any) {
    this.foodOrderData.getData('foodItems.json').subscribe(result => {
      this.foodData = result.find( ele => ele.foodId === foodId);
    });
  }

  foodItemAddEvent(event) {
    this.foodItemsService.addRemoveFoodItem(event);
  }

}
