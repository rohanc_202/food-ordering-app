import { Injectable } from '@angular/core';
import { CommunicationService } from 'src/app/shared/services/communication/communication.service';

@Injectable({
  providedIn: 'root'
})
export class FoodItemsService {

  checkoutFoodList = new Set();

  constructor(private communication: CommunicationService) { }

  addRemoveFoodItem(foodItem: any) {

    if (foodItem) {
        if (this.checkoutFoodList.has(foodItem)) {
        this.checkoutFoodList.delete(foodItem);
        if (foodItem.count > 0) {
          this.checkoutFoodList.add(foodItem);
        }
      } else {
        this.checkoutFoodList.add(foodItem);
      }
        this.communication.sendFoodCheckoutData(this.checkoutFoodList);
    }
  }
}
