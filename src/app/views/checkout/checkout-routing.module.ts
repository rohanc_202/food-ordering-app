import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CheckoutListComponent } from './checkout-list/checkout-list.component';
import { OrderSuccessComponent } from './order-success/order-success.component';


const routes: Routes = [
  {
    path: '',
    component: CheckoutListComponent
  },
  {
    path: 'order-success',
    component: OrderSuccessComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckoutRoutingModule { }
