import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CheckoutRoutingModule } from './checkout-routing.module';
import { CheckoutListComponent } from './checkout-list/checkout-list.component';
import { OrderSuccessComponent } from './order-success/order-success.component';


@NgModule({
  declarations: [CheckoutListComponent, OrderSuccessComponent],
  imports: [
    CommonModule,
    CheckoutRoutingModule
  ]
})
export class CheckoutModule { }
