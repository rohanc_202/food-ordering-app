import { Component, OnInit } from '@angular/core';
import { CommunicationService } from 'src/app/shared/services/communication/communication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkout-list',
  templateUrl: './checkout-list.component.html',
  styleUrls: ['./checkout-list.component.scss']
})
export class CheckoutListComponent implements OnInit {

  checkoutFoodList: any;
  totalBill = 0;
  deliveryCharge = 10;

  constructor(private communication: CommunicationService, private route: Router) { }

  ngOnInit() {
    this.communication.getFoodCheckoutData().subscribe(foodData => {
      if (foodData) {
        this.checkoutFoodList = foodData;
        this.calculateTotal(this.checkoutFoodList);
      }
    });
  }

  calculateTotal(checkoutFoodList: any) {
    checkoutFoodList.forEach(element => {
      this.totalBill += element.price * element.count;
    });
  }

  checkout() {
    this.route.navigate(['/checkout/order-success']);
  }

}
