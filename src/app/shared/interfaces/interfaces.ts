export interface Food {
    id: number;
    type: string;
    description: string;
    img: string;
}
