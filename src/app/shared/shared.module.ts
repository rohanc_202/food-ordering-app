import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {AuthGuard} from './guard/auth.guard';
import { FootItemComponent } from './components/foot-item/foot-item.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

@NgModule({
  declarations: [FootItemComponent, PageNotFoundComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers : [AuthGuard],
  exports: [ FormsModule, ReactiveFormsModule, HttpClientModule, FootItemComponent, PageNotFoundComponent ]
})
export class SharedModule { }
