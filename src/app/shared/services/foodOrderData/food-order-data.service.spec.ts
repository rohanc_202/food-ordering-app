import { TestBed } from '@angular/core/testing';

import { FoodOrderDataService } from './food-order-data.service';

describe('FoodOrderDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FoodOrderDataService = TestBed.get(FoodOrderDataService);
    expect(service).toBeTruthy();
  });
});
