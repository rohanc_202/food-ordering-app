import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FoodOrderDataService {

  baseUrl = 'assets/json/';

  constructor(private http: HttpClient) { }

  getData(apiUrl: any) {
    const url = this.baseUrl + apiUrl;
    return this.http.get<any>(url);
  }

  addData(payload: any, apiUrl: any): Observable<any> {
    const url = this.baseUrl + apiUrl;
    return this.http.post<any>(url, payload);
  }
}
