import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import routingUrls from '../../json/routingUrls.json';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private route: Router) { }

  getAccessToken() {
    return localStorage.getItem('token');
  }

  setAccessToken(token: any) {
    localStorage.setItem( 'token', token );
  }

  isUserLoggedIn() {
    if (localStorage.getItem('token')) {
      return true;
    } else {
      return false;
    }
  }

  logout() {
    localStorage.clear();
    sessionStorage.clear();
    this.route.navigate([routingUrls.LOGIN]);
  }
}
