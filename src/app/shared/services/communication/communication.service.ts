import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  private foodCheckoutDataSubject = new BehaviorSubject<any>(null);

  constructor() { }

  sendFoodCheckoutData(data: any) {
    this.foodCheckoutDataSubject.next(data);
  }

  getFoodCheckoutData() {
    return this.foodCheckoutDataSubject.asObservable();
  }
}
