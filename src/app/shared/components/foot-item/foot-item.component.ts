import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CommunicationService } from '../../services/communication/communication.service';

@Component({
  selector: 'app-foot-item',
  templateUrl: './foot-item.component.html',
  styleUrls: ['./foot-item.component.scss']
})
export class FootItemComponent implements OnInit {

  @Input() foodData: any;
  @Output() foodItemAddEvent = new EventEmitter<string>();
  checkoutFoodList: any;

  constructor(private communication: CommunicationService) { }

  ngOnInit() {
    this.foodData.count = 0;
    this.checkIfItemInCart();
  }

  addFoodItem() {
    this.foodData.count++;
    this.foodItemAddEvent.emit(this.foodData);
  }

  checkIfItemInCart() {
    this.communication.getFoodCheckoutData().subscribe(foodData => {
      if (foodData) {
        foodData.forEach(element => {
          if (element.foodId === this.foodData.foodId && element.id === this.foodData.id) {
            this.foodData = element;
          }
        });
      }
    });
  }

  removeFoodItem() {
    if (this.foodData.count > 0) {
      this.foodData.count--;
      this.foodItemAddEvent.emit(this.foodData);
    }
  }
}
