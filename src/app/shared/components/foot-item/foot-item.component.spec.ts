import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FootItemComponent } from './foot-item.component';

describe('FootItemComponent', () => {
  let component: FootItemComponent;
  let fixture: ComponentFixture<FootItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FootItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FootItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
