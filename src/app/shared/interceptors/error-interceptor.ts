import { Injectable, Injector } from '@angular/core';
import {
  HttpEvent, HttpRequest, HttpHandler,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {Router} from '@angular/router';
import { AuthService } from '../services/authService/auth.service';
import routingUrls from './../json/routingUrls.json';

@Injectable()
export class ServerErrorInterceptor implements HttpInterceptor {

  private refreshTokenInProgress = false;
    // Refresh Token Subject tracks the current token, or is null if no token is currently
    // available (e.g. refresh pending).
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private router: Router, private injector: Injector, private authService: AuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      return next.handle(request).pipe(
          catchError((error: any) => {
              if (error instanceof HttpErrorResponse && error.status === 401) {
                    sessionStorage.clear();
                    localStorage.clear();
                    this.router.navigate( [routingUrls.LOGIN] );
                    return throwError(error);
              } else if ( error.status === 500) {
                this.router.navigate( [ routingUrls.ERROR_500] );
                return throwError('Internal server error');
              } else  {
                return throwError('Something went wrong');
              }
          })
      );
  }
}
