import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppContainerComponent } from './container/app-container/app-container.component';
import { LoginComponent } from './views/login/login.component';
import { AuthGuard } from './shared/guard/auth.guard';
import { InternalServerErrorComponent } from './views/internal-server-error/internal-server-error.component';
import { PageNotFoundErrorComponent } from './views/page-not-found-error/page-not-found-error.component';


const routes: Routes = [
  {
    path: '',
    component: AppContainerComponent, canActivate: [AuthGuard],
    children: [
        {
            path: '',
            loadChildren: () => import('./views/food-dashboard/food-dashboard.module').then(m => m.FoodDashboardModule)
        },
        {
          path: 'checkout',
          loadChildren: () => import('./views/checkout/checkout.module').then(m => m.CheckoutModule)
        },
        {
          path: 'error-500',
          component: InternalServerErrorComponent
        }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'error-404',
    component: PageNotFoundErrorComponent
  },
  {
    path: '**',
    redirectTo: '/error-404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
